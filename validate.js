import { showMessage, clearMessage } from "./controller-admin.js";

function checkBlank(userInput, id) {
  if (userInput.length == 0) {
    showMessage(id, "Field không được để trống!");
    return false;
  } else {
    clearMessage(id);
    return true;
  }
}

function checkProductType(userInput, id) {
  let isValidType = userInput == "Samsung" || userInput == "iPhone";
  if (isValidType) {
    clearMessage(id);
    return true;
  } else {
    showMessage(id, "Loại sản phẩm không hợp lệ!");
    return false;
  }
}

function checkIsNum(userInput, id) {
  var reg = /\D/;

  let isNum = reg.test(userInput);
  if (!isNum) {
    clearMessage(id);
    return true;
  } else {
    showMessage(id, "Giá tiền phải là số!");
    return false;
  }
}

function checkIsImageURL(userInput, id) {
  var reg = /http[^\?]*.(jpeg|jpg|png)$/;

  let isImgURL = reg.test(userInput);
  if (isImgURL) {
    clearMessage(id);
    return true;
  } else {
    showMessage(id, "Sai định dạng link & Link phải chứa hình ảnh!");
    return false;
  }
}

export function validateProduct(data) {
  let isValid = true;

  //validate name
  isValid = checkBlank(data.name, "tbTenSP");

  //validate type
  isValid = isValid & checkProductType(data.type, "tbLoaiSP");

  //validate price
  isValid =
    isValid &
    (checkBlank(data.price, "tbGiaSP") && checkIsNum(data.price, "tbGiaSP"));

  //validate screen
  isValid = isValid & checkBlank(data.screen, "tbLoaiMH");

  //validate front camera
  isValid = isValid & checkBlank(data.frontCamera, "tbCamTruoc");

  //validate back camera
  isValid = isValid & checkBlank(data.backCamera, "tbCamSau");

  //validate image
  isValid =
    isValid &
    (checkBlank(data.img, "tbHinhSP") && checkIsImageURL(data.img, "tbHinhSP"));

  //validate desc
  isValid = isValid & checkBlank(data.desc, "tbMoTa");

  return isValid;
}
